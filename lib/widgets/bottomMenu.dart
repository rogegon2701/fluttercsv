import 'package:flutter/material.dart';
import 'package:flutter_application_1/config/routes.dart';

class DesignFlutterBottomMenu extends StatelessWidget {
  final int menuIndex;

  DesignFlutterBottomMenu(this.menuIndex);

  Color colorByIndex(ThemeData theme, int index) {
    return index == menuIndex ? theme.accentColor : theme.primaryColor;
  }

  BottomNavigationBarItem getItem(
      Icon image, String title, ThemeData theme, int index) {
    return BottomNavigationBarItem(icon: image, label: title
        // title: Text(
        //   title,
        //   style: TextStyle(
        //     fontSize: 10.0,
        //     color: colorByIndex(theme, index),
        //   ),
        // ),
        );
  }

  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(15), topLeft: Radius.circular(15)),
        boxShadow: [
          BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
        ],
      ),
      child: Theme(
        data: Theme.of(context).copyWith(
          // sets the background color of the `BottomNavigationBar`
          canvasColor: Theme.of(context).primaryColor,
          // sets the active color of the `BottomNavigationBar` if `Brightness` is light
          primaryColor: Theme.of(context).accentColor,
          textTheme: Theme.of(context).textTheme.copyWith(
                caption: TextStyle(color: Colors.black54),
              ),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15.0),
            topRight: Radius.circular(15.0),
          ),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: menuIndex,
            onTap: (value) {
              switch (value) {
                case 0:
                      Navigator.of(context).pushNamedAndRemoveUntil(
                      GiftRoutes.home, (Route<dynamic> route) => false);
                  break;
                case 1:
                      Navigator.of(context).pushNamedAndRemoveUntil(
                      GiftRoutes.stats, (Route<dynamic> route) => false);
                  break;
              }
            },
            items: bartItemButton(_theme),
          ),
        ),
      ),
    );
  }

  bartItemButton(ThemeData theme) {
    List<BottomNavigationBarItem> barItem = [
          getItem(Icon(Icons.home), 'Home', theme, 0),
          getItem(Icon(Icons.insights), 'Stats', theme, 1)
    ];
    

    return barItem;
  }
}
