import 'package:flutter/material.dart';
import 'package:flutter_application_1/widgets/bottomMenu.dart';
class DesignFlutterScaffold extends StatelessWidget {
  final Color background;
  final String title;
  final Widget body;
  final int bottomMenuIndex;
  final List<String> tabBarList;
  final TabController tabController;
  final ValueNotifier<int> badgetCount;

  const DesignFlutterScaffold(
      {Key key,
      this.background,
      @required this.title,
      @required this.body,
      @required this.bottomMenuIndex,
      this.tabBarList,
      this.tabController,
      this.badgetCount,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var tabBars = <Tab>[];
    var _theme = Theme.of(context);
    if (tabBarList != null) {
      for (var i = 0; i < tabBarList.length; i++) {
        tabBars.add(Tab(key: UniqueKey(), text: tabBarList[i]));
      }
    }
    return Scaffold(
      backgroundColor: background,
      appBar: whichAppBar(context, title, _theme),
      resizeToAvoidBottomPadding: false,
      body: body,
      bottomNavigationBar: DesignFlutterBottomMenu(bottomMenuIndex),
    );
  }

  Widget whichAppBar(BuildContext context, String title, ThemeData theme) {
    Widget appBar;

    switch (title) {
      case 'Home':
        appBar = AppBar(
          title: Text('Home'),
          centerTitle: true,
          automaticallyImplyLeading: false,
          
        );
        break;
      case 'Stats':
        appBar = AppBar(
          title: Text("Stats"),
          automaticallyImplyLeading: false,
          centerTitle: true,
        );
        break;
    }

    return appBar;
  }

  
}
