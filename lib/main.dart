import 'dart:core';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:csv/csv.dart';
import 'dart:convert' show utf8;
import 'package:flutter_application_1/config/callMessageServices.dart';
import 'package:flutter_application_1/widgets/scaffold.dart';
import 'package:google_fonts/google_fonts.dart';

import 'config/themeConfig.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lista de clientes',
      debugShowCheckedModeBanner: false,
      theme: themeData(ThemeConfig.lightTheme),
      darkTheme: themeData(ThemeConfig.darkTheme),
      home: TableLayout(),
    );
  }

  ThemeData themeData(ThemeData theme) {
    return theme.copyWith(
      textTheme: GoogleFonts.sourceSansProTextTheme(
        theme.textTheme,
      ),
    );
  }
}


class TableLayout extends StatefulWidget {
  @override
  _TableLayoutState createState() => _TableLayoutState();
}

class _TableLayoutState extends State<TableLayout> {
  List<List<dynamic>> data = [];
  final CallsAndMessagesService _service = CallsAndMessagesService();
  loadAsset(List<List<dynamic>> path) async {
    List<List<dynamic>> csvTable = path;
    print(csvTable);
    data = csvTable;
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {

    return SafeArea(
        child: DesignFlutterScaffold(
      background: null,
      title: 'Home',
      body: homePage(context),
      bottomMenuIndex: 0,
    ));

    
  }

  Widget homePage(BuildContext context){
    
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add, color: Colors.white),
          backgroundColor: Colors.blue,
          onPressed: () async {
            
            FilePickerResult result = await FilePicker.platform.pickFiles(type: FileType.custom, 
            allowedExtensions: ['csv, xlxs']);

            if(result != null) {
              final input = new File(result.files.single.path).openRead();
              final fields = await input.transform(utf8.decoder).transform(new CsvToListConverter()).toList();
              await loadAsset(fields);
            } else {
              print("Error al seleccionar archivo");
            }
          }),
      body: 
      SingleChildScrollView(
      child: Center(  
              child: Column(children: <Widget>[  
                Container(  
                  margin: EdgeInsets.all(20),  
                  child: Table(  
                    defaultColumnWidth: FixedColumnWidth((MediaQuery.of(context).size.width - 10) / 3),  
                    border: TableBorder.all(  
                        color: Colors.black,  
                        style: BorderStyle.solid,  
                        width: 1),  
                    children: generateList(data),  
                  ),  
                ),  
              ])  
          ), )
      
    );
  }

  List<TableRow> generateList(List<List<dynamic>> data){
    List<TableRow> lista = <TableRow>[];

    lista.add(
      TableRow( children: [  
                        Column(children:[Text('Cliente', style: TextStyle(fontSize: 17.0))]),  
                        Text('Numero', style: TextStyle(fontSize: 17.0), textAlign: TextAlign.center),  
                        Text('Actions', style: TextStyle(fontSize: 17.0), textAlign: TextAlign.center),  
                      ])
    );

    data.forEach((List<dynamic> item) =>{
      lista.add(
        TableRow( children: [  
                        Text(item[0], style: TextStyle(fontSize: 15.0), textAlign: TextAlign.center),   
                        Center(child: Text('${item[1]}', style: TextStyle(fontSize: 15.0))),  
                        Center(child:Container(
                                    width: 50,
                                    child: RaisedButton(
                                      shape: StadiumBorder(),
                                      color: Colors.green.shade200,
                                      onPressed: () {
                                        _service.call('${item[1]}');
                                        print("llamada");
                                      },
                                      child: Icon(Icons.phone, size: 20.0,),
                                    ),
                                  ),),  
                      ])
      )
    });

    return lista;

  }
}