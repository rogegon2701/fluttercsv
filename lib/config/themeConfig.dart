import 'package:flutter/material.dart';

class ThemeConfig {
  //Colors for theme
  static Color lightPrimary = Colors.blue;
  //static Color darkPrimary = Color(0xff1f1f1f);
  //static Color darkPrimary = Color(0xff203890);
  static Color darkPrimary = Color(0xfff063057);

  static Color lightAccent = Colors.grey[50];
  //static Color darkAccent = Colors.lightBlue;
  static Color darkAccent = Color(0xfffcfcff);

  static Color lightBG = Colors.grey[200];
  static Color darkBG = Color(0xff121212);
  //static Color darkBG = Color(0xffeeb32b);
  static Color badgeColor = Colors.red;
  static Color greyColor = Colors.black;
  static Color backgroundCard =
      Color.fromRGBO(64, 75, 96, .9); //Colors.grey.shade400;
  static Color errorColor = Colors.red.shade900;
  //Color.fromRGBO(64, 75, 96, .9)

  static ThemeData lightTheme = ThemeData(
    backgroundColor: lightBG,
    primaryColor: lightPrimary,
    accentColor: lightAccent,
    cursorColor: lightAccent,
    scaffoldBackgroundColor: lightBG,
    appBarTheme: AppBarTheme(
      elevation: 0,
      textTheme: TextTheme(
        headline6: TextStyle(
          color: darkAccent,
          fontSize: 18.0,
          fontWeight: FontWeight.w800,
        ),
      ),
    ),
  );

  static ThemeData darkTheme = ThemeData(
    brightness: Brightness.dark,
    backgroundColor: darkBG,
    primaryColor: darkPrimary,
    accentColor: darkAccent,
    scaffoldBackgroundColor: darkBG,
    cursorColor: darkAccent,
    buttonTheme: ButtonThemeData(
      buttonColor: Colors.deepPurple, //  <-- light color
      textTheme: ButtonTextTheme.primary, //  <-- dark text for light background
    ),
    appBarTheme: AppBarTheme(
      elevation: 0,
      textTheme: TextTheme(
        headline6: TextStyle(
          color: lightBG,
          fontSize: 18.0,
          fontWeight: FontWeight.w800,
        ),
      ),
    ),
  );
}
